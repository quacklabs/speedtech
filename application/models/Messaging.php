<?php 
defined("BASEPATH") or die("Direct script access not allowed");

class Messaging extends CI_Model{


  public function __construct(){
    parent::__construct();
  }

  public function run($action, $data = null){
    return $this->$action($data);
  }


  public function mark_read($id){
    return $this->db->query("UPDATE `messaging` SET `read_status` = 1 WHERE `id` = '{$id}' ");
  }
  public function get_msg($id){
    return  $this->db->query("SELECT * FROM `messaging` WHERE `id` = {$id}")->row();
  }


  public function send_message($payload){
    $this->db->insert('messaging', $payload);
  }

  public function get_all($type="all", $user = NULL, $limit = 0, $start = 1){
    if($user == NULL){
      $user = $this->admin->current_user;
    }
    switch($type){
      case "unread":
        $condition = "`read_status` = 0 AND `recipient_id` = '{$user}'";
        break;
      case "read":
        $condition = "`read_status` = 1 AND `recipient_id` = '($user}'";
        break;
      case "inbox":
        $condition = "`recipient_id` = '{$user}'";
        break;
      case "sent":
        $condition = "`sender_id` = '{$user}'";
        break;
      case "all":
        $condition ="`id` AND `recipient_id` = '{$user}'";
        break;
    }

    $query = $this->db->query("SELECT * FROM `messaging` WHERE ({$condition}) ORDER BY `id` DESC");
    return $query->result();
  }
}
?>