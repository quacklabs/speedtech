<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
  User Model
*/

class User extends CI_Model{
  public $error;

  public $current_user;

  public function __construct(){
    parent::__construct();
    $this->load->library("email");
    $this->load->library('parser');
    $this->clean_queue();
  }


  //MARK: Verify OTP
  public function verify_otp($phone, $otp, $user){
    $this->db->select('id');
    $this->db->where('phone', $phone);
    $this->db->where('code', $otp);
    $this->db->where('status', 0);
    
    //$this->db->query("SELECT `id` WHERE `phone` = '{$phone}' AND `code` = '{$otp}' AND `status` = 0 LIMIT 1");
    if($this->db->get('verification')->num_rows() < 1){
      $this->error = "Invalid Code/Phone number";
      return true;
    }else{
      $id = $this->db->get('verification');
      $this->db->set('status', 1)->where('id', $id->row('id'))->update('verification');
      $this->db->set('email_verified', 1)->where('id', $user)->update('users');
      $this->error = "ok";
      return true;
    }
  }


  // Get a single user
  public function get_user($id){
    
    if(!is_numeric($id)){
      $query = $this->db->select('*')->from('users')->where('users.msg_id', $id)->join('details', 'details.user_id = users.id')->get();//$this->db->query("SELECT `users`.*, `details`.* FROM `users` RIGHT OUTER JOIN `details` ON `details`.`user_id` = `users`.`id` WHERE (`users`.`msg_id` = '{$id}')");

    }else{
      $query = $this->db->select('*')->from('users')->where('users.id', $id)->join('details', 'details.user_id = users.id')->get();//$this->db->query("SELECT `users`.*, `details`.* FROM `users` RIGHT OUTER JOIN `details` ON `details`.`user_id` = `users`.`id` WHERE (`users`.`id` = '{$id}')");
    }


    // return $id;
    return $query->result();
  }

  //update a user
  public function update($type, $data, $id){
    switch($type){
      case "detail":
        $this->db->set($data);
        $this->db->where('user_id', $id);
        $query = $this->db->update('details');
        break;
      case "user":
        $this->db->set($data);
        $this->db->where('id', $id);
        $query = $this->db->update('users');
        break;
    }

    if(!$query){
      $this->error ="failed";
    }else{
      $this->error = "Success";
    }

    return true;
  }

  //Get all users from database
  public function get_all($type="all"){
    switch($type){
      case "verified":
        $condition = "`email_verified` = 1";
        break;
      case "pending":
        $condition = "`email_verified` = 0";
        break;
      case "all":
        $condition = "id";
        break;
    }
    
    $query = $this->db->query("SELECT * FROM `users` WHERE ({$condition}) ORDER BY `id` ASC");
    return $query->result();
  }
  

  //Login user
  public function login($email, $password, $type = 'user'){

    switch($type){
      case 'user':
        $query = $this->db->query("SELECT * FROM `users` WHERE `email` = '{$email}'");
        if($query->num_rows() < 1){
          $this->error = "User Not Found";
        }else{
          $user = $query->row();
          if(!password_verify($password, $user->password)){
            $this->error = "Invalid login details provided";
          }else{
            if($user->status == 1){
              $tokenData = array();
              $tokenData['id'] = $user->id;
              $session_array = array(
                "token" => AUTHORIZATION::generateToken($tokenData),
                "logged_in" => true,
              );
              $this->session->set_userdata($session_array);
              $this->error = $this->session->userdata("token");
              $this->db->set(array('last_login' => time()))->where('id', $user->id)->update('users');
              $this->db->set('last_login', time())->where('id', $user->id)->update('users');
              //$this->
              return true;
            }else{
              $this->error = "Account Inactive";
              return false;
            }
          }
        }
        break;
      case  'admin':
        //$this->session->sess_destroy();
        $query = $this->db->query("SELECT * FROM `admin` WHERE `username` = '{$email}'");
        if($query->num_rows() < 1){
          $this->error = "Admin Not Found";
          return false;
        }else{
          $user = $query->row();
          if(!password_verify($password, $user->password_hash)){
            return false;
          }else{
            $tokenData = array();
            $tokenData['user_id'] = $user->id;
            $tokenData['username'] = $user->username;
            $tokenData['timestamp'] = time();
            $session_array = array(
              "token" => AUTHORIZATION::generateToken($tokenData),
              "logged_in" => true,
            );
            $this->session->set_userdata($session_array);
            $this->error = "ok";
            return true;
            
          }
        }
        break;
      default:
        $this->login($email, $password, 'user');
    }
    
  }

  //Register User
  public function register($user_details){
    
    $this->db->trans_start();

    if($this->db->insert('users', array("email" => $user_details['email'], "password" => password_hash($user_details['password'], PASSWORD_DEFAULT), "date_created" => strtotime('now'), "msg_id" => strtotime('now'), "ref_id" => $user_details['ref_id']))){
      $details = array(array_diff($user_details, array("email", "pass")));
      $details = array(
        "user_id" => $this->db->insert_id(),
        "phone" => $user_details["phone"],
        "first_name" => $user_details["first_name"],
        "last_name" => $user_details["last_name"]
      );
      $add_details = $this->db->insert('details', $details);
      if($add_details === TRUE){
        $this->db->trans_complete();
        $this->error = $this->send_verification($user_details['phone'], $user_details['first_name']);
        return true;
      }else{
        $this->db->trans_rollback();
        $this->error = "An unknown error has occured, user creation failed";
        return false;
      }
    }else{
      $this->db->trans_rollback();
      $this->error = "User could not be created, please try again with a different email";//$this->error = $this->db->error();
      return false;
    }


  }


  public function clean_queue(){

  }


  private function send_verification($phone, $codeName){
    //Transform our POST array into a URL-encoded query string.
      $code = mt_rand(111111,999999);
      $verification_data = array(
        "phone" => $phone,
        "code" => $code
      );

      $this->db->insert('verification', $verification_data);

      $owneremail="marcfrancy2k3@yahoo.com";
      $subacct="BARKU";
      $subacctpwd="barku100";
      $sendto = "{$verification_data['phone']}";
      $sender="BARKU";
      $message="Dear {$codeName}, Please enter the following SMS Verifiation Code as follows: {$code}, to verify your phone number.";
      
      $url = "http://www.smslive247.com/http/index.aspx";
      $payload = array(
        "cmd" => "sendquickmsg",
        "owneremail" => $owneremail,
        "subacct" => $subacct,
        "subacctpwd" => $subacctpwd,
        "message" => $message,
        "sendto" => $sendto,
        "sender" => $sender,
        "msgtype" => 0
      ); 

      $msg = http_build_query($payload);

      //$url = $url.$msg;

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $url. (strpos($url, '?') === FALSE ? '?' : '') . $msg);

      //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0); 
      curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
      if( ! $result = curl_exec($ch)) 
      { 
          trigger_error(curl_error($ch)); 
      } 
      curl_close($ch); 
      return $result; 
      
      //Print the data out onto the page.
      //return $data;

      // if ($f = @fopen($url, "r"))  { 
      //   $answer = fgets($f, 255); 
      //   if (substr($answer, 0, 1) == "+") {  
      //     return "SMS to $dnr was successful.";  
      //   }  else  {  
      //     return "an error has occurred: [$answer]."; 
      //   }  
      // }  else  {  
      //  return "Error: URL could not be opened.";  
      // } 
      
			// $fields = array(
      //   "first_name" => $user["first_name"],
      //   "code" => $code
			// );

      // $string = $this->parser->parse('verify', $fields, TRUE);
      
      // $config = array();
      // $config["protocol"] = "smtp";
      // $config["smtp_host"] = "mail.saharahace.com";
      // $config["smtp_port"] = 465;
      // $config["smtp_user"] = "no-reply@saharahace.com";
      // $config["smtp_pass"] = "Xolikore19@";
      // $config["smtp_crypto"] = "ssl";
      // $this->email->initialize($config);

			// $this->email->from("no-reply@saharahace.com", "Barku Verification");
			// $this->email->to($user["email"]);
			// $this->email->subject('Barku Email Verification');
			// $this->email->set_mailtype('html');
      // $this->email->message($string);
      // $this->email->send();
  }


  public function is_logged_in($mode = "api", $token = null){
    $login_status = false;
    switch($mode){

      case "api":
        $check = AUTHORIZATION::validateToken($token);
        if(!$check){
          $login_status = false;
        }else{
          $this->current_user = $check->id;
          $login_status = true;
        }
        break;
      
      case "admin":
        if($this->session->is_logged_in == TRUE){

        }
        break;

    }
    
    return $login_status;
    

  }





  public function logout(){
    $this->session->sess_destroy();
    return true;
  }


}

?>