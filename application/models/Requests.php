<?php
defined("BASEPATH") or die("Direct script access not allowed");


class Requests extends CI_Model{
  
  public function __construct(){
    parent::__construct();
  }

  public function get_all($type ="all", $status = 0){
    switch($type){
      case 'withdrawal':
        $request_type = "`request_type` = 'withdrawal' AND `request_status` = '{$status}'";
        break;
      case 'donation':
        $request_type = "`request_type` = 'donation' AND `request_status` = '{$status}'";
        break;
    }

    $query = $this->db->query("SELECT * FROM `requests` WHERE ({$request_type}) ORDER BY `id` ASC");
    return $query->result();
  }
}

?>