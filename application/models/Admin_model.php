<?php

defined("BASEPATH") or die("Direct script access is not allowed");


class Admin_model extends CI_Model{

  public $error;

  public $current_user;

  public function __function(){
    parent::__construct();
  }

  public function isLoggedIn(){
    if($this->session->userdata('logged_in') == true){
      $token_string = $this->session->userdata('token');
      if(AUTHORIZATION::validateTimestamp($token_string) != false){
        $payload = AUTHORIZATION::validateTimestamp($token_string);
        if($payload != false && $this->is_admin($payload->user_id, $payload->username)){
          $this->current_user = $payload->username;
          return true;
        }
      }    
    }
    return false;
  }

  private function is_admin($user_id, $username){

    $query = $this->db->query("SELECT * FROM `admin` WHERE `id` = '{$user_id}' AND `username` = '{$username}' ORDER BY `id`");

    if($query->num_rows() < 1){
      return false;
    }

    return true;

  }


}


?>