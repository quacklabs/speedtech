<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
  Testing user models and logic
*/


class User_tests extends CI_Model{

  private $ci;
  public function __construct(){
    // $this->ci  =& get_instance();
    parent::__construct();
    $this->load->library("unit_test");
  }

  public function run($func, $data){
    return $this->_func($data);
  }
  
  public function authentication($data){
    $data = json_decode($data);
    $type = ($data->get != null) ? $data->get->case : 'all';
    $this->load->model('user');
    $inputs = array(
      "valid_login" => array("user" => "test@gmail.com", "pass" => "Xolikore19@"),
      "invalid_login" => array("user" => "makr@nndn", "pass" => "123"),
      "valid_admin_login" => array("user" => "admin", "pass" => "Xolikore19@"),
      "invalid_admin_login" => array("user" => "makr@nndn", "pass" => "123")
    );

    switch($type){
      case "all":
        $this->unit->run($this->user->login_admin($inputs['invalid_login']), false, "invalid login returns false");
        break;
      case 'login':
        // User login
        $this->unit->run($this->user->login($inputs['invalid_login']['user'], $inputs['invalid_login']['pass'], 'user'), false, "invalid user login returns false :".$this->user->error);
        $this->unit->run(($this->user->login($inputs['valid_login']['user'], $inputs['valid_login']['pass'], 'user')== true), true, "valid user login returns true :".$this->user->error);
        // Admin login
        $this->unit->run($this->user->login($inputs['invalid_admin_login']['user'], $inputs['invalid_admin_login']['pass'], 'admin'), false, "invalid admin login returns false :".$this->user->error);
        $this->unit->run($this->user->login($inputs['valid_admin_login']['user'], $inputs['valid_admin_login']['pass'], 'admin'), true, "valid admin login returns true :".$this->user->error);
        
        
        break;
      case 'register':
        $this->db->trans_start();
        $this->db->query('TRUNCATE TABLE `users`');
        $this->db->query('TRUNCATE TABLE `details`');
        $this->db->trans_complete();
        $inputs['valid_login']['pass'] = password_hash($inputs['valid_login']['pass'], PASSWORD_DEFAULT);
        $details = array_merge($inputs['valid_login'], array("phone" => "08004183098", 'first_name' => 'Arya', 'last_name' => 'stark'));

        $this->unit->run($this->user->register($details), true, "valid registration succeed: ".$this->user->error);
        $this->db->trans_start();
        $user = $this->db->query("SELECT * FROM `details` WHERE `user_id` = 1");
        $this->db->trans_complete();
        $this->unit->run(($user->num_rows() == 1), true, "User details were added ok");
        break;
      default:
        $this->unit->run($this->user->login($inputs['invalid_login']), false, "invalid login returns false");
    }

    //Login Test
    return $this->unit->report();

  }


  public function __destruct(){

  }
}
?>