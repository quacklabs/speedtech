<?php
defined("BASEPATH") or die("Direct script access not allowed");
?>

<!-- Footer -->
<footer class="footer">
  <div class="row align-items-center justify-content-xl-between">
    <div class="col-xl-6">
      <div class="copyright text-center text-xl-left text-muted">
        &copy; 
        <script>
          document.write(new Date().getFullYear());
        </script> 
        <a href="#" class="font-weight-bold ml-1" target="_blank">Prof. E</a>
      </div>
    </div>

    <script>
      $(function(){
        $(".info-modal").click(function(e){
          e.preventDefault()
          alert("open modal")
        })
        //   alert("open modal")
        // }
      });
    
    
    </script>
    <!-- <div class="col-xl-6">
      <ul class="nav nav-footer justify-content-center justify-content-xl-end">
        <li class="nav-item">
          <a href="#" class="nav-link" target="_blank">Creative Tim</a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link" target="_blank">About Us</a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link" target="_blank">Blog</a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link" target="_blank">MIT License</a>
        </li>
      </ul>
    </div> -->
  </div>
</footer>