<?php
defined("BASEPATH") or die("Direct script access not allowed");
?>

<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Total Verified Users</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo count($data->users->verified); ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                        <i class="fas fa-chart-bar"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="text-uppercase text-muted mb-0">Total Pending Users</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo count($data->users->pending);?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                        <i class="fas fa-chart-pie"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Donation Requests</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo count($data->requests->donation);?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                        <i class="fas fa-users"></i>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Withdrawal Request</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo count($data->requests->withdrawal);?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-percent"></i>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

</div>

<div class="container-fluid mt--7">

  <div class="row mt-5">
        <div class="col-xl-6 mb-5 mb-xl-0">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Donation Request List</h3>
                </div>
                <div class="col text-right">
                  <a href="#" class="btn btn-sm btn-primary info-modal" data-requestid="donation">See all</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col text-uppercase">Full Name</th>
                    <th scope="col text-uppercase">Phone</th>
                    <th scope="col text-uppercase">Amount</th>
                    <th scope="col text-uppercase">Date</th>
                  </tr>
                </thead>
                <tbody>

                  <?php 
                    if(count($data->requests->donation) > 0){
                      foreach($data->requests->donation as $row){
                  ?>
                      <tr>
                        <th scope="row">
                          /argon/
                        </th>
                        <td>
                          4,569
                        </td>
                        <td>
                          340
                        </td>
                        <td>
                          <i class="fas fa-arrow-up text-success mr-3"></i> 46,53%
                        </td>
                      </tr>
                  <?php 
                      }
                    }else{ 
                        echo '<tr><th scope="row">No Donation Requests Found</th></tr>';
                    } 
                  ?>
                  
                  
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>


        <div class="col-xl-6">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Withdrawal Request List</h3>
                </div>
                <div class="col text-right">
                  <a href="#" class="btn btn-sm btn-primary info-modal" data-requestid="withdrawal">See all</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col text-uppercase">Full Name</th>
                    <th scope="col text-uppercase">Phone</th>
                    <th scope="col text-uppercase">Amount</th>
                    <th scope="col text-uppercase">Date</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    if(count($data->requests->withdrawal) > 0){
                      foreach($data->requests->withdrawal as $row){
                  ?>
                      <tr>
                        <th scope="row">
                          /argon/
                        </th>
                        <td>
                          4,569
                        </td>
                        <td>
                          340
                        </td>
                        <td>
                          <i class="fas fa-arrow-up text-success mr-3"></i> 46,53%
                        </td>
                      </tr>
                  <?php 
                      }
                    }else{ 
                        echo '<tr><th scope="row">No Withdrawal Requests Found</th></tr>';
                    } 
                  ?>
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
  <?php echo $footer; ?>

</div>