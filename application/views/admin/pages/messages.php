<?php
defined("BASEPATH") or die("DIrect script access not allowed");
?>

<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-6 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Inbox</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo number_format(count($data->messages->inbox))//number_format(count($data->inbox)); ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                        <i class="fas fa-chart-bar"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-6 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="text-uppercase text-muted mb-0">Sent Messages</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo number_format(count($data->messages->sent));?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                        <i class="fas fa-chart-pie"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

</div>

<div class="container-fluid mt--7">

  <div class="row mt-5">
    <div class="col-xl-12 mb-5 mb-xl-0">
      <div class="card shadow card-custom bg-grey">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Mailbox <span class="text-muted h6"><?php echo count($data->messages->unread);?> unread messages</span></h2>
            </div>
          </div>
        </div>

        <div class="card-body row col-xl-12">
          <div class="col-xl-3">
            <button class="btn btn-block mb-xl-4" data-toggle="modal" data-target="#exampleModal">Compose</button>

            <div class="nav pills tableview flex-column mt-10" id="v-nav-pills-tab" role="tablist" aria-orientation="vertical">
              <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-inbox" role="tab" aria-controls="v-pills-inbox" aria-selected="true"><i class="ni ni-box-2"></i> Inbox <?php echo count($data->messages->unread);?></a>
              <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-sent" role="tab" aria-controls="v-pills-sent" aria-selected="false"><i class="ni ni-email-83"></i> Sent</a>
              <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-draft" role="tab" aria-controls="v-pills-draft" aria-selected="false"><i class="ni ni-single-copy-04"></i> Draft</a>
              <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-junk" role="tab" aria-controls="v-pills-junk" aria-selected="false"><i class="fa fa-filter"></i> Junk</a>
              <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-trash" role="tab" aria-controls="v-pills-trash" aria-selected="false"><i class="fa fa-trash"></i> Trash</a>
            </div>
          
          </div>


          <div class="col-xl-9">
              <div class="tab-content tableview listview" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-inbox" role="tabpanel" aria-labelledby="v-pills-inbox-tab">
                  <div class="table-responsive">
                    <!-- INBOX table -->
                    <table class="table table-custom align-items-center table-flush">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col text-uppercase"><input type="checkbox"></th>
                          <th scope="col text-uppercase">Sender Name</th>
                          <th scope="col text-uppercase">Subject</th>
                          <th scope="col text-uppercase">Date</th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php 
                        // var_dump($data->messages->inbox);
                          if(count($data->messages->inbox) > 0){
                            foreach($data->messages->inbox as $row){
                              $user = $this->user->get_user($row->sender_id);
                              // var_dump($user);
                        ?>
                            <tr class="<?php echo ($row->read_status == 0) ? 'bold' : '';?>">
                              <th scope="row">
                                <div class="form-check">
                                  <input class="form-check-input select" type="checkbox" value="<?php echo $row->id; ?>" id="defaultCheck1">
                                </div>
                              </th>
                              <th>
                                <?php echo $user[0]->first_name." ".$user[0]->last_name; ?>
                              </th>
                              <td>
                                <a href="#" class="open-detail" data-row='<?php echo json_encode($this->view->obj2arr($row)); ?>' ><?php echo $row->subject;?></a>
                              </td>
                              <td>
                                <?php echo date("d-m-Y H:m", strtotime($row->date_created));?>
                              </td>
                              <!-- <td>
                                <i class="fas fa-arrow-up text-success mr-3"></i> 46,53%
                              </td> -->
                            </tr>
                        <?php 
                            }
                          }else{ 
                              echo '<tr><th scope="row">No Messages Found</th></tr>';
                          } 
                        ?>
                        
                        
                        
                      </tbody>
                    </table>
                  </div>
                </div>


                <div class="tab-pane" id="v-pills-sent" role="tabpanel" aria-labelledby="v-pills-sent-tab">
                  <div class="table-responsive">
                    <!-- Sent table -->
                    <table class="table align-items-center table-flush">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col text-uppercase"><div type="checkbox"></div></th>
                          <th scope="col text-uppercase">Sender Name</th>
                          <th scope="col text-uppercase">Subject</th>
                          <th scope="col text-uppercase">Date</th>
                        </tr>
                      </thead>
                      <tbody>
                          
                        <?php
                          if(count($data->messages->sent) > 0){
                            foreach($data->messages->sent as $row){
                              $user = $this->user->get_user($row->sender_id)
                        ?>
                            <tr class="<?php echo ($row->read_status == 0) ? 'bold' : '';?>">
                              <th scope="row">
                                <div class="form-check">
                                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                  <label class="form-check-label" for="defaultCheck1">
                                    Default checkbox
                                  </label>
                                </div>
                              </th>
                              <th>
                                <?php echo $user->first_name." ".$user->last_name; ?>
                              </th>
                              <td>
                                <a href="#" class="open-detail" data-row='<?php echo json_encode($row) ?>'><?php echo $row->subject;?></a>
                              </td>
                              <td>
                                <?php echo date("d-m-Y H:m", strtotime($row->date_created));?>
                              </td>
                              <!-- <td>
                                <i class="fas fa-arrow-up text-success mr-3"></i> 46,53%
                              </td> -->
                            </tr>
                        <?php 
                            }
                          }else{ 
                              echo '<tr><th scope="row">No Messages Found</th></tr>';
                          } 
                        ?>
                        
                        
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>  
               


            <div class="tableview detail">
              <div class="card card-custom bg-white">
                <div class="card-header border-0">
                  <div class="row">
                    <h2 class="mb-0">Read Mail</h2>
                    <a href="#" class="ml-auto text-muted pull-right close"><i class="fa fa-times"></i></a>
                  </div>
                </div>

                <div class="card-body">
                  <div class="row">
                    <h4 class="subject"></h4><br />
                  </div>
                  <div class="row">
                    <h5 class="text-muted sender"></h5>
                  </div>
                  <div class="row">
                    <div class="body col-xl-12">
                    
                    </div>                          
                  </div>
                
                </div>
              
              </div>
            </div>     
          </div>
        
        </div>

      </div>
    </div>
  </div>
  <?php echo $footer; ?>

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog tableview card-custom" role="document">
    <div class="modal-content full">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Compose Message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-block-sm btn-danger" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-block-sm btn-primary btn-theme">Send</button>
      </div>
    </div>
  </div>
</div>


<script>
$(function(){
  var selected = []

  // $(".select").on("click", function(e){
  //   //e.preventDefault();

  //   if($(this).attr("checked") == true){
  //     alert("check time");
  //   }else{
  //     alert("unchecked");
  //   }
  //   //alert("take over");
  // })
  $("a[class='open-detail']").click(function(e){
    e.preventDefault();
    var data = $(this).data("row");
    showMessage(data);
    markRead(data.id);
    $(this).parent("tr").removeClass("bold");
  })

  $(".nav-link").click(function(e){
    $(".listview").show();
    $(".detail").hide();
  })


  $(".close").click(function(e){
    e.preventDefault();
    $(".listview").show();
    $(".detail").hide();
  })


  // new $.fn.dataTable.Buttons( table, {
  //     buttons: [
  //         'copy',
  //         { extend: 'excel', text: 'Save as Excel' }
  //     ]
  // } );


  function showMessage($message_data){
    // var messageView = ;
    $(".listview").hide();
    $(".detail").show();
    $(".detail .subject").html("Subject: " + $message_data.subject);
    $(".detail .sender").html("from: " + $message_data.sender_id);
    $(".detail .body").html($message_data.message);
  }

  function markRead(id){
    $.ajax({
      url: "<?php echo base_url('api/message/mark-read'); ?>",
      type: "POST",
      data: {"id": id},
      dataType: "JSON",
      beforeSend: function(xhr){xhr.setRequestHeader('Authorization', 'Bearer <?php echo $this->session->token ?>');},
      success: function(resp){
        console.log(resp)
      },
      error: function(xhr,status, error){
        console.log(error)
      }
    })
  }
})


</script>