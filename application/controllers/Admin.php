<?php
defined("BASEPATH") or die("Direct script access is not allowed");

class Admin extends CI_Controller{


  private $template = array();
  private $data;// = new stdClass();


  public function __construct(){
    parent::__construct();
    $this->load->model('admin_model', 'admin');
    $this->load->library('parser');
    $this->load->model('view_data', 'view');
    $this->template["footer"] = $this->parser->parse('admin/footer', array(), true);
    $this->data = new stdClass();
  }



  public function index(){
    if(!$this->admin->isLoggedIn()){
      //$this->template['page'] = 'login'
      redirect(base_url().'admin/login', 'refresh', 301);
    }else{
      redirect(base_url().'admin/dashboard', 'refresh', 301);
    }
  }


  //Dashboard
  public function dashboard(){
    
    if(!$this->admin->isLoggedIn()){
      $this->login();
    }else{
      // $view = new ;
      //$data = new stdClass();
      $this->data->users = $this->view->get_objc('user', array("pending", "verified", "all"));
      $this->data->requests = $this->view->get_objc("requests", array("donation", "withdrawal"));

      $view_data = array(
        "page" => "dashboard", 
        "title" => "My Admin Dashboard",
        "data" => $this->data
      );

      $this->_render($view_data);
    }
  }

  //Users management
  public function users(){
    if(!$this->admin->isLoggedIn()){
      return $this->login();
    }else{
      $get_data = $this->uri->segment(3);
      // $page = ($)
      //$data = new stdClass();

      $this->data->users = $this->user->get_all("verified");//$this->user->get_all(array("verified"));

      $view_data = array(
        "page" => "users",
        "title" => "Manage Users",
        "data" => $this->data
      );

      $this->_render($view_data);
    }
  }

  //payments views
  public function payments(){
    if(!$this->admin->isLoggedIn()){
      return $this->login();
    }else{


      $view_data = array(
        "page" => "payments",
        "title" => "Manage Payments"
      );



      $this->_render($view_data);
    }
  }


  // Settings Page
  public function settings(){
    if(!$this->admin->isLoggedIn()){
      return $this->login();
    }else{
      //$data = new stdClass();

      $view_data = array(
        "page" => "settings",
        "title" => "Customize Settings"
      );

      $this->_render($view_data);
    }
  }


  // Messages Page
  public function messages(){
    if(!$this->admin->isLoggedIn()){
      return $this->login();
    }else{

      $this->data->messages = $this->view->get_objc('messaging', array("inbox", "sent", "unread"));
      //$this->data->sent = $this->view->get_objc('messaging', array("sent"));
      $view_data = array(
        "page" => "messages",
        "title" => "Messages",
        "data" => $this->data
      );

      $this->_render($view_data);
    }
  }

  //Active Donations
  public function active_donation(){
    if(!$this->admin->isLoggedIn()){
      return $this->login();
    }else{
      $this->data->active_donations = $this->view->get_objc('requests', array('donation'));

      $view_data = array(
        "page" => "active_donations",
        "title" => "Manage Payments - Active Donations",
        "data" => $this->data
      );

      $this->_render($view_data);
    }
  }


  // Withdrawal requests
  public function active_withdrawal($type="all"){
    if(!$this->admin->isLoggedIn()){
      return $this->login();
    }else{
      $this->data->active_donations = $this->view->get_objc('requests', array('withdrawal'));

      $view_data = array(
        "page" => "withdrawal_requests",
        "title" => "Manage Payments - Withdrawal Requests",
        "data" => $this->data
      );

      $this->_render($view_data);
    }
  }

  // Failed transactions
  public function failed_transactions(){
    if(!$this->admin->isLoggedIn()){
      return $this->login();
    }else{
      $this->data->failed_payments = $this->view->get_objc('payment', array('withdrawal'));
      $this->data->failed_requests = $this->view->get_objc('requests', array('withdrawal'));

      $view_data = array(
        "page" => "failed_transactions",
        "title" => "Manage Payments - Withdrawal Requests",
        "data" => $this->data
      );

      $this->_render($view_data);
    }
  }
  


  //Logout
  public function logout(){
    $this->user->logout();
    return $this->login();
  }


  //Login Page
  public function login(){
    $view_data = array('page' => 'login');
    if($_POST && !empty($_POST)){
      $this->form_validation->set_rules('username', 'Username', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required');

      if($this->form_validation->run() == true){
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        if($this->user->login($user, $pass, 'admin')){
          redirect(base_url().'admin/dashboard', 'refresh', 301);
        }
        $this->template["error"] = $this->user->error;
      }else{
        $this->template["error"] = true;
      }
    }


    $this->_render($view_data);
  }


  private function _render($view_data){
    $this->template["user"] = AUTHORIZATION::validateToken($this->session->token);
    $this->template = array_merge($this->template, $view_data);
      //$this->template['options']['other_pages'] = TRUE;
    $this->load->view("admin/render", $this->template);
  }


}



?>