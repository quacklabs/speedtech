<?php

class Api extends CI_Controller{

  public $token;
  
  public function __construct(){
    parent::__construct();
    $this->_init();
  }

  private function _init(){
    $this->load->model('user');
  }


  public function hash_password($password){
    echo json_encode(array("hash" => password_hash($password, PASSWORD_DEFAULT)));
  }

  public function index(){
    $headers = apache_request_headers();
    $head = "";
    foreach ($headers as $header => $value) {
        $head .= "$header: $value <br />\n";
    }
    $response = array(
      "status" => 200,
      "header" => $head
    );

    echo json_encode($response);
  }


  public function user($action = "get", $id = NULL){
    $response = array();
    if(!$this->is_authorized()){
      http_response_code(403);
      $response["error"] = http_response_code();
    }else{
      
      if($id == NULL){
        $id = $this->user->current_user;
      }
      switch($action){
        case "get":
          $user = $this->user->get_user($id);
          $response["user"] = ($user != NULL) ? $user : "User not found";
          //$response
          break;
        case "update-detail":
          $updates = $this->input->post();
          $data = array();
          foreach($updates as $key => $value){
            $data["$key"] = $value;
          }
          $this->user->update("detail", $data, $id);
          $response['error'] = $this->user->error;
          break;
      }
    }

    echo json_encode($response);
  }

  public function login(){
    $response = array("status" => 200);
    $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
    $this->form_validation->set_rules('pass', 'Password', 'required|min_length[8]');

    if($this->form_validation->run() == TRUE){
      $username = $this->input->post('email');
      $password = $this->input->post('pass');
      $login = $this->user->login($username, $password, 'user');

      if($login == true){
        $response['error'] = false;
        $response['token'] = $this->user->error;
      }else{
        $response['error'] = true;
        $response['msg'] = $this->user->error; 
      }
    }else{
      $response['error'] = true;
      $response['msg'] = validation_errors(); 
    }
    
    echo json_encode($response);    
  }

  function vefify_email(){

  }

  function register(){
    $resp_arr = array();
    if($_POST){
      $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[users.email]');
      $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
      $this->form_validation->set_rules('phone', 'Phone Number', 'required|min_length[11]|max_length[11]');
      $this->form_validation->set_rules('first_name', 'First Name', 'required');
      $this->form_validation->set_rules('last_name','Last Name', 'required');

      if($this->form_validation->run() == TRUE){
        $form_data = $this->input->post();
        $register = $this->user->register($form_data);
        if($register == TRUE){

          $resp_arr['error'] = $this->user->error;
          $login = $this->user->login($form_data['email'], $form_data['password'], 'user');
          if(!$login){
            $resp_arr['error'] = "failed";
          }else{
            $resp_arr['error'] = "ok";
          }
        }else{
          $resp_arr['error'] = "failed";
        }
        $resp_arr['msg'] = $this->user->error;
      }else{
        $resp_arr['error'] = "failed";
        $resp_arr['msg'] = validation_errors();
      }
    }else{
      $resp_arr['error'] = "failed";
      $resp_arr['msg'] = 'Invalid Request Type: (GET)';
    }
    echo json_encode($resp_arr);
  }


  //OTP Verification
  public function verify_otp($phone, $otp){
    $response = array();
    if(!$this->is_authorized()){
      http_response_code(403);
      $response["error"] = http_response_code();
    }else{
      $user = AUTHORIZATION::validateToken($this->token);
      $this->user->verify_otp($phone, $otp, $user->id);
      $response["error"] = $this->user->error;
    }

    echo json_encode($response);
  }

  public function verify_account(){
    
  }

  public function message($action = "all"){
    $response = array();
    if(!$this->is_authorized()){
      http_response_code(403);
      $response["error"] = http_response_code();
    }else{
      $this->load->model('messaging');
      
      $data = ($this->input->post() != NULL) ? $this->input->post() : NULL;
      
      switch($action){
        case 'get_all':
          $response['messages'] = $this->messaging->get_all($this->user->current_user);
        case 'mark_read':
          $response['error'] = ($this->messaging->mark_read($data["id"]) == true) ? "success" : "operation failed";
          break;
      }
    }
    

    echo json_encode($response);

  }


  private function is_authorized(){
    $this->load->model("admin_model");
    $headers = apache_request_headers();
    $this->token = substr($headers['Authorization'], 7);
    return $this->user->is_logged_in("api", $this->token);
    
    
   // echo json_encode($status); //$this->user->is_logged_in("api", substr($headers['Authorization'], 7));
  }

  public function logout(){
    $this->session->sess_destroy();
    echo json_encode(array("error" => "success"));
  }
}

?>